select 
prod.Name as Product_name,
sel.Name +' '+ sel.Surname as Surname_Name , 
(sum(sal.Quantity)/ prod.Quantity * 100) as Sales_percentage
from 
Sellers as sel
left join Sales sal on sal.IDSel = sel.ID
join (
	select 
	prod.ID, 
	prod.Name, 
	sum(sal.Quantity) as Quantity
	from Arrivals arriv
	left join Products prod on prod.ID = arriv.IDProd
	left join Sales sal on sal.IDProd = prod.ID and sal.Date BETWEEN '01.10.2013' AND '07.10.2013'
	where arriv.Date BETWEEN '07.09.2013' AND '07.10.2013'
	group by prod.ID, prod.Name
	) as prod on prod.ID = sal.IDProd
where sal.Date BETWEEN '01.10.2013' AND '07.10.2013'
group by sel.Name, sel.Surname, prod.Name, prod.Quantity
order by prod.Name, sel.Name, sel.Surname
