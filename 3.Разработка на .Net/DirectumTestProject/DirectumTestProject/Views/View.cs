﻿namespace DirectumTestProject.Views
{
    public class View : IView
    {
        public void PrintMessage(string message)
        {
            Console.WriteLine(message);
        }

        public string? RequiredCommand()
        {
            Console.WriteLine("Введите комманду");
            return Console.ReadLine();
        }
    }
}
