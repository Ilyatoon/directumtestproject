﻿namespace DirectumTestProject.Views
{
    public interface IView
    {
        void PrintMessage(string message);
        string? RequiredCommand();
    }
}
