﻿using DirectumTestProject.Models;
using DirectumTestProject.Presenters;
using DirectumTestProject.Service.DomainService;
using DirectumTestProject.Views;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

var host = Host.CreateDefaultBuilder().ConfigureServices(services => {
    services.AddSingleton<IPresenter, Presenter>();
    services.AddScoped<IView, View>();
    services.AddScoped<IMeetingService, MeetingService>();
    services.AddScoped<INotificationService, NotificationService>();
    services.AddScoped<IModel, Model>(x=> new Model(x.GetRequiredService<IMeetingService>(), x.GetRequiredService<INotificationService>()));
})
.Build();

var presenter = host.Services.GetRequiredService<IPresenter>();