﻿using DirectumTestProject.Consts;
using DirectumTestProject.DataSource.Domain;
using DirectumTestProject.Dto;
using DirectumTestProject.Service.DomainService;
using System.Globalization;

namespace DirectumTestProject.Models
{
    public class Model : IModel
    {
        private readonly IMeetingService _meetingService;
        private readonly INotificationService _notificationService;

        private IEnumerable<NotificationDto> _notifications;

        public Model(IMeetingService meetingService, INotificationService notificationService)
        {
            _meetingService = meetingService;
            _notificationService = notificationService;
            UpdateNotifications();
        }

        private void UpdateNotifications()
        {
            _notifications = _notificationService.GetActiveNotificationDtos();
        }

        public IEnumerable<string> CheckNotification(DateTime dateTime)
        {
            if (!_notifications.Any(x => x.NotificationTime <= dateTime))
                return null;

            var indices = _notifications.Where(x => x.NotificationTime <= dateTime).Select(x => x.Id).ToArray();
            var meetings = _meetingService.GetMeetingsByIndices(indices);
            _notificationService.SetWasNotificate(indices);
            UpdateNotifications();
            return meetings?.Select(ConvertMeetingToShortString).ToArray();
        }

        public IEnumerable<string> GetAllMeeting()
        {
            var meetings = _meetingService.GetAll();
            return meetings?.Select(ConvertMeetingToString).ToArray();
        }

        public IEnumerable<string> GetMeetingsAfterDate(DateTime date)
        {
            var meetings = _meetingService.GetMeetingsAfterDate(date);
            return meetings?.Select(ConvertMeetingToString).ToArray();
        }

        public Meeting GetMeetingById(int meetingId)
        {
            return _meetingService.GetMeetingById(meetingId);
        }

        public void SaveMeeting(Meeting meeting)
        {
            _meetingService.SaveMeeting(meeting);
        }

        public bool TryAddMeeting(string[] parameters)
        {
            if (!parameters.Any())
                return false;

            if (!TryGetData(parameters[0], parameters[1], out var startTime))
                return false;
            if (!TryGetData(parameters[2], parameters[3], out var finishTime))
                return false;

            if (!CheckTimeAvailability(startTime, finishTime))
                return false;

            var meeting = new Meeting()
            {
                StartTime = startTime,
                FinishTime = finishTime,
            };

            if (parameters.Length < 6 || !TryGetData(parameters[4], parameters[5], out var notificateDate))
            {
                SaveMeeting(meeting);
                return true;
            }

            meeting.Notification = new Notification() { NotificationTime = notificateDate, Meeting = meeting };
            SaveMeeting(meeting);
            UpdateNotifications();
            return true;
        }

        private bool CheckTimeAvailability(DateTime startTime, DateTime finishTime)
        {
            return finishTime > startTime && !_meetingService.CheckMeetingExists(startTime, finishTime);
        }

        public bool DeleteMeeting(string[] parameters)
        {
            if (!parameters.Any() || !TryGetInt(parameters[0], out var meetingId))
                return false;

            var meeting = _meetingService.GetMeetingById(meetingId);
            if (meeting is null)
                return false;

            _meetingService.DeleteMeeting(meeting);
            UpdateNotifications();
            return true;
        }

        public bool ChangeExistingMeeting(string[] parameters)
        {
            if (!parameters.Any())
                return false;

            if (!TryGetInt(parameters[0], out var meetingId))
                return false;

            var meeting = GetMeetingById(meetingId);
            if (meeting is null)
                return false;

            if (!TryGetData(parameters[1], parameters[2], out var startTime))
                return false;
            if (!TryGetData(parameters[3], parameters[4], out var finishTime))
                return false;

            if (!CheckTimeAvailability(startTime, finishTime))
                return false;


            meeting.StartTime = startTime;
            meeting.FinishTime = finishTime;
            if (parameters.Length < 6 || !TryGetData(parameters[5], parameters[6], out var notificateDate))
            {
                _meetingService.UpdateMeeting(meeting);
                return true;
            }

            if (meeting.Notification is null)
                meeting.Notification = new Notification() { NotificationTime = notificateDate, Meeting = meeting };
            else
                meeting.Notification.NotificationTime = notificateDate;

            _meetingService.UpdateMeeting(meeting);
            UpdateNotifications();


            return true;
        }

        public bool ExportToFile(string[] parameters)
        {
            if (!parameters.Any())
                return false;

            if (!TryGetDataDay(parameters[0], out var startTime))
                return false;

            var meetings = _meetingService.GetMeetingsByDay(startTime);
            string path = Environment.CurrentDirectory;

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(path, $"{parameters[0]}.txt"), false))
            {
                foreach (var meeting in meetings)
                    outputFile.WriteLine(ConvertMeetingToString(meeting));
            }

            return true;
        }

        private bool TryGetInt(string text, out int value)
        {
            return int.TryParse(text, NumberStyles.None, CultureInfo.InvariantCulture, out value);
        }

        private bool TryGetData(string dateString, string timeString, out DateTime dateTime)
        {
            return DateTime.TryParseExact($"{dateString} {timeString}", DateStringFormatConst.FullDateStringFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);
        }

        private bool TryGetDataDay(string dateString, out DateTime dateTime)
        {
            return DateTime.TryParseExact(dateString, DateStringFormatConst.ShortDateStringFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);
        }

        private string ConvertMeetingToString(Meeting meeting)
        {
            return $"{meeting.Id} {meeting.StartTime.ToString(DateStringFormatConst.FullDateStringFormat)} " +
                $"{meeting.FinishTime.ToString(DateStringFormatConst.FullDateStringFormat)} {meeting.Notification?.NotificationTime}";
        }

        private string ConvertMeetingToShortString(Meeting meeting)
        {
            return $"{meeting.Id}; с {meeting.StartTime.ToString(DateStringFormatConst.FullDateStringFormat)} до {meeting.FinishTime.ToString(DateStringFormatConst.FullDateStringFormat)}";
        }

    }
}
