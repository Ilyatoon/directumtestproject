﻿using DirectumTestProject.DataSource.Domain;

namespace DirectumTestProject.Models
{
    public interface IModel
    {
        IEnumerable<string> GetAllMeeting();
        IEnumerable<string> GetMeetingsAfterDate(DateTime date);
        Meeting GetMeetingById(int meetingId);
        bool TryAddMeeting(string[] parameters);
        bool ChangeExistingMeeting(string[] parameters);
    
        void SaveMeeting(Meeting meeting);
        bool DeleteMeeting(string[] parameters);
        bool ExportToFile(string[] parameters);
        IEnumerable<string> CheckNotification(DateTime dateTime);
    }
}
