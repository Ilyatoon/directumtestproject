﻿using DirectumTestProject.Consts;
using DirectumTestProject.Models;
using DirectumTestProject.Views;
using NHibernate.Util;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Timers;

namespace DirectumTestProject.Presenters
{
    public class Presenter : IPresenter
    {
        private readonly IView _view;
        private readonly IModel _model;

        private const string headerstring = "ID Время начала    Время окончания  Время оповещения";

        private const string addMeetingSuccessMessage = "Встреча добавлена";
        private const string addMeetingFailureMessage = "Не удалось добавить встречу";

        private const string changeExistingMeetingSuccessMessage = "Встреча обновлена";
        private const string changeExistingMeetingFailureMessage = "Не удалось обновить встречу";

        private const string deleteMeetingSuccessMessage = "Встреча удалена";
        private const string deleteMeetingFailureMessage = "Не удалось удалить встречу";

        private const string exportToFileSuccessMessage = "Встречи экспортированы в файл";
        private const string exportToFileFailureMessage = "Не удалось экспортировать встречи";

        private const string addCommandCaption = "add";
        private const string showAllCommandCaption = "showall";
        private const string showActualCommandCaption = "showactual";
        private const string changeCommandCaption = "change";
        private const string deleteCommandCaption = "delete";
        private const string exportCommandCaption = "export";
        private const string helpCommandCaption = "help";

        private System.Timers.Timer notificationTimer;

        public Presenter(IView view, IModel model)
        {
            _view = view;
            _model = model;

            StartWork();
        }

        private void StartWork()
        {
            _view.PrintMessage($"Для просмотра доступных команд введите \"{helpCommandCaption}\"\nБлижайшие встречи");
            ShowActualMeetings();
            SetTimer();
            RequiredCommand();
        }

        private void SetTimer()
        {
            notificationTimer = new System.Timers.Timer();
            notificationTimer.Interval = 60000;

            notificationTimer.Elapsed += new ElapsedEventHandler(TimeTick);
            notificationTimer.Start();
        }


        private void TimeTick(object? sender, ElapsedEventArgs e)
        {
            var notificationInfo = _model.CheckNotification(e.SignalTime);
            if (notificationInfo is not null)
                foreach (var notification in notificationInfo)
                    _view.PrintMessage($"Скоро новая встреча: {notification}");
        }

        private void RequiredCommand()
        {
            var requiredString = _view.RequiredCommand();
            var parameters = requiredString.Split(new char[0], StringSplitOptions.RemoveEmptyEntries).ToArray();
            var commandKey = parameters?.Length > 0 ? parameters[0] : string.Empty;
            SetAction(commandKey, parameters.Skip(1).ToArray());


            RequiredCommand();
        }



        private void SetAction(string commandKey, string[] parameters)
        {
            switch (commandKey)
            {
                case addCommandCaption:
                    TryAddMeeting(parameters);
                    return;
                case showAllCommandCaption:
                    ShowAllMeetings();
                    return;
                case showActualCommandCaption:
                    ShowActualMeetings();
                    return;
                case changeCommandCaption:
                    ChangeExistingMeeting(parameters);
                    return;
                case deleteCommandCaption:
                    DeleteMeeting(parameters);
                    return;
                case exportCommandCaption:
                    ExportToFile(parameters);
                    return;
                case helpCommandCaption:
                    ShowAvailableCommands();
                    return;
            }
        }

        private void ShowAvailableCommands()
        {
            _view.PrintMessage($"{showAllCommandCaption} - Отображает все встречи");
            _view.PrintMessage($"{showActualCommandCaption} - Отображает все будущие встречи");
            _view.PrintMessage($"{addCommandCaption} дата_начала дата_окончания [дата_уведомления] - Добавить встречу. Даты указываются в формате {DateStringFormatConst.FullDateStringFormat}");
            _view.PrintMessage($"{changeCommandCaption} id_встречи дата_начала дата_окончания [дата_уведомления] - Изменить встречу. Даты указываются в формате {DateStringFormatConst.FullDateStringFormat}");
            _view.PrintMessage($"{deleteCommandCaption} id_встречи - Удалить встречу");
            _view.PrintMessage($"{exportCommandCaption} указанный_день - Экспорт встреч за указанный день в текстовый файл. Дата указываются в формате {DateStringFormatConst.FullDateStringFormat}");
            _view.PrintMessage($"{helpCommandCaption} - Список доступных команд");
        }

        private void ExportToFile(string[] parameters)
        {
            PrintResultMessage(_model.ExportToFile(parameters), exportToFileSuccessMessage, exportToFileFailureMessage);
        }



        private void TryAddMeeting(string[] parameters)
        {
            PrintResultMessage(_model.TryAddMeeting(parameters), addMeetingSuccessMessage, addMeetingFailureMessage);
        }

        private void ChangeExistingMeeting(string[] parameters)
        {
            PrintResultMessage(_model.ChangeExistingMeeting(parameters), changeExistingMeetingSuccessMessage, changeExistingMeetingFailureMessage);
        }
        private void DeleteMeeting(string[] parameters)
        {
            PrintResultMessage(_model.DeleteMeeting(parameters), deleteMeetingSuccessMessage, deleteMeetingFailureMessage);
        }

        private void PrintResultMessage(bool condition, string successMessage, string failureMessage)
        {
            if (condition)
                _view.PrintMessage(successMessage);
            else
                _view.PrintMessage(failureMessage);
        }

        private void ShowActualMeetings()
        {
            PrintMeetingInfo(_model.GetMeetingsAfterDate(DateTime.Now.Date));
        }

        private void ShowAllMeetings()
        {
            PrintMeetingInfo(_model.GetAllMeeting());
        }

        private void PrintMeetingInfo(IEnumerable<string> meetingsInfo)
        {
            if (!meetingsInfo.Any())
            {
                _view.PrintMessage("Не найдено");
                return;
            }

            _view.PrintMessage(headerstring);
            foreach (var meeting in meetingsInfo)
                _view.PrintMessage(meeting);
        }
    }
}
