﻿using DirectumTestProject.DataSource.DataBase;
using DirectumTestProject.DataSource.Domain;
using DirectumTestProject.DataSource.Repository;
using NHibernate;

namespace DirectumTestProject.Service.DomainService
{
    public class MeetingService : IMeetingService
    {
        public readonly IRepository<Meeting> repository = new MeetingRepository();

        public IEnumerable<Meeting> GetActualMeeting()
        {
            var currentDate = DateTime.Now;
            using (ISession session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Meeting>()
                    .Where(p => p.StartTime > currentDate)
                    .OrderBy(p => p.StartTime)
                    .ToArray();
            }
        }


        public IEnumerable<Meeting> GetMeetingsAfterDate(DateTime date)
        {
            using (ISession session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Meeting>()
                    .Where(p => p.StartTime > date)
                    .OrderBy(p => p.StartTime)
                    .ToArray();
            }
        }

        public IEnumerable<Meeting> GetAll()
        {
            using (ISession session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Meeting>()
                    .OrderBy(p => p.StartTime)
                    .ToArray();
            }
        }

        public Meeting GetMeetingById(int meetingId)
        {
            using (ISession session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Meeting>().FirstOrDefault(p => p.Id == meetingId);
            }
        }

        public IEnumerable<Meeting> GetMeetingsByDay(DateTime startTime)
        {
            using (ISession session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Meeting>()
                    .Where(p => p.StartTime > startTime && p.StartTime < startTime.AddDays(1))
                    .OrderBy(p => p.StartTime)
                    .ToArray();
            }
        }

        public bool CheckMeetingExists(DateTime startTime, DateTime finishTime)
        {
            using (ISession session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Meeting>()
                    .Any(p => (finishTime > p.StartTime && finishTime < p.FinishTime)
                        || (startTime > p.StartTime && startTime < p.FinishTime));
            }
        }

        public IEnumerable<Meeting> GetMeetingsByIndices(int[] indices)
        {
            using (ISession session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Meeting>()
                    .Where(x => indices.Contains(x.Id))
                    .ToArray();

            }
        }

        public void UpdateMeeting(Meeting meeting)
        {
            repository.Update(meeting);
        }

        public void DeleteMeeting(Meeting meeting)
        {
            repository.Delete(meeting);
        }
        public void SaveMeeting(Meeting meeting)
        {
            repository.Save(meeting);
        }
    }
}
