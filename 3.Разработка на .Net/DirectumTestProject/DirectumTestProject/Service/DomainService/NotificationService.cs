﻿using DirectumTestProject.DataSource.DataBase;
using DirectumTestProject.DataSource.Domain;
using DirectumTestProject.DataSource.Repository;
using DirectumTestProject.Dto;
using NHibernate;

namespace DirectumTestProject.Service.DomainService
{
    public class NotificationService : INotificationService
    {
        public readonly IRepository<Notification> repository = new NotificationRepository();
        public IEnumerable<NotificationDto> GetActiveNotificationDtos()
        {
            using (ISession session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Notification>()
                    .Where(p => !p.WasNotificate)
                    .OrderBy(p => p.NotificationTime)
                    .Select(ConvertToDto)
                    .ToArray();
            }
        }

        public IEnumerable<Notification> GetNotificationsByIndices(int[] indices)
        {
            using (ISession session = NHibernateHelper.GetCurrentSession())
            {
                return session.Query<Notification>()
                    .Where(p => indices.Contains(p.Id))
                    .ToArray();
            }
        }

        public void SetWasNotificate(int[] indices)
        {

            var notifications = GetNotificationsByIndices(indices);
            foreach (var notification in notifications)
            {
                notification.WasNotificate = true;
                repository.Update(notification);
            }

        }

        private NotificationDto ConvertToDto(Notification notification)
        {
            return new NotificationDto(notification.Id, notification.NotificationTime);
        }
    }
}
