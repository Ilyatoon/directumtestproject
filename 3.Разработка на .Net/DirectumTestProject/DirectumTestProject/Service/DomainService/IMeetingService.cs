﻿using DirectumTestProject.DataSource.Domain;

namespace DirectumTestProject.Service.DomainService
{
    public interface IMeetingService
    {
        bool CheckMeetingExists(DateTime startTime, DateTime finishTime);
        void DeleteMeeting(Meeting meeting);
        IEnumerable<Meeting> GetActualMeeting();
        IEnumerable<Meeting> GetAll();
        Meeting GetMeetingById(int meetingId);
        IEnumerable<Meeting> GetMeetingsAfterDate(DateTime date);
        IEnumerable<Meeting> GetMeetingsByDay(DateTime startTime);
        IEnumerable<Meeting> GetMeetingsByIndices(int[] indices);
        void SaveMeeting(Meeting meeting);
        void UpdateMeeting(Meeting meeting);
    }
}