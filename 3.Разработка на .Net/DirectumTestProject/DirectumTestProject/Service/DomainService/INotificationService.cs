﻿using DirectumTestProject.DataSource.Domain;
using DirectumTestProject.Dto;

namespace DirectumTestProject.Service.DomainService
{
    public interface INotificationService
    {
        IEnumerable<NotificationDto> GetActiveNotificationDtos();
        IEnumerable<Notification> GetNotificationsByIndices(int[] indices);
        void SetWasNotificate(int[] indices);
    }
}