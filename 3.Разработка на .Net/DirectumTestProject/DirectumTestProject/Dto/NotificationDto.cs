﻿namespace DirectumTestProject.Dto
{
    public struct NotificationDto
    {
        public NotificationDto(int id, DateTime notificationTime)
        {
            Id = id;
            NotificationTime = notificationTime;
        }

        public int Id { get; }
        public DateTime NotificationTime { get; }
    }
}
