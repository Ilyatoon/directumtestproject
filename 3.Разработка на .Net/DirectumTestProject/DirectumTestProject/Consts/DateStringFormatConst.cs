﻿namespace DirectumTestProject.Consts
{
    public class DateStringFormatConst
    {
        public const string FullDateStringFormat = "dd.MM.yyyy HH:mm";
        public const string ShortDateStringFormat = "dd.MM.yyyy";
    }
}
