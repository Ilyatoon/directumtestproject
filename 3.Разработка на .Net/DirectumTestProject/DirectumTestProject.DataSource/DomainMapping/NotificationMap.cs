﻿using DirectumTestProject.DataSource.Domain;
using FluentNHibernate.Conventions.Inspections;

namespace DirectumTestProject.DataSource.DomainMapping
{
    public class NotificationMap : EntityMap<Notification>
    {
        public NotificationMap()
        {
            Table("Meeting_Notification");

            Id(x => x.Id, "Id").GeneratedBy.Identity();
            Map(w => w.NotificationTime, "Notificate_Time");
            Map(w => w.WasNotificate, "Was_Notificate");
            
            
            HasOne(x => x.Meeting).Constrained();
        }
    }
}
