﻿using DirectumTestProject.DataSource.Domain;
using NHibernate.Mapping;

namespace DirectumTestProject.DataSource.DomainMapping
{
    public class MeetingMap : EntityMap<Meeting>
    {
        public MeetingMap()
        {
            Table("Meeting");

            Id(x => x.Id, "Id").GeneratedBy.Identity();
            Map(w => w.StartTime, "Start_Time");
            Map(w => w.FinishTime, "Finish_Time");

            HasOne(x=> x.Notification).Cascade.All();
        }
    }
}
