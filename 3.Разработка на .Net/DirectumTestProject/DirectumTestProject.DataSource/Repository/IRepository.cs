﻿using DirectumTestProject.DataSource.Domain;
namespace DirectumTestProject.DataSource.Repository
{
    public interface IRepository<T> where T : EntityBase
    {
        void Save(T entity);
        void Update(T entity);
        void Delete(T entity);

    }
}
