﻿using DirectumTestProject.DataSource.DataBase;
using DirectumTestProject.DataSource.Domain;
using NHibernate;

namespace DirectumTestProject.DataSource.Repository
{
    public abstract class Repository<T> : IRepository<T> where T: EntityBase
    {
        public void Delete(T entity)
        {
            using (ISession session = NHibernateHelper.GetCurrentSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Delete(entity);
                transaction.Commit();
            }
        }

        public void Save(T entity)
        {
            using (ISession session = NHibernateHelper.GetCurrentSession())
            using (ITransaction transaction = session.BeginTransaction())
            {

                session.Save(entity);
                transaction.Commit();
                transaction.Dispose();
            }
        }

        public void Update(T entity)
        {
            using (ISession session = NHibernateHelper.GetCurrentSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Update(entity);                
                transaction.Commit();
            }
        }
    }
}
