﻿namespace DirectumTestProject.DataSource.Domain
{
    public class Notification : EntityBase
    {
        public virtual Meeting Meeting { get; set; }
        public virtual DateTime NotificationTime { get; set; }
        public virtual bool WasNotificate { get; set; }
    }
}
