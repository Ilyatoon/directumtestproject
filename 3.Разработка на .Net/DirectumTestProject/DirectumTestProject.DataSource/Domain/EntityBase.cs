﻿namespace DirectumTestProject.DataSource.Domain
{
    public abstract class EntityBase
    {
        public virtual int Id { get; set; }
    }
}
