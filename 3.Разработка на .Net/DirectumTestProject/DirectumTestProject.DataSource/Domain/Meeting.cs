﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DirectumTestProject.DataSource.Domain
{
    public class Meeting : EntityBase
    {
        public virtual DateTime StartTime { get; set; }
        public virtual DateTime FinishTime { get; set; }
        public virtual Notification Notification { get; set; }
    }
}
